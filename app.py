# TwilioCon Leaderboard by
# Gowri Grewal
# Team Hackbright - September 2013
# Go team 7
"""
model.py has the Class and model for this app


OBJECTIVE:

Retrieve leaderboard stored in Hacker Olympics API
Display in a table.

"""
from flask import Flask, render_template, redirect, request
import model

app = Flask(__name__)


#this may be an opportunity to do screen scraping later...
@app.route("/")
def index():
	leaderboard = model.load_leaderboard()
	return render_template("index.html", leaderboard=leaderboard)

  
if __name__== "__main__":
    app.run(debug=True)
